# Straver

This website allows you to visualise and manipulate your training data (mostly from swim sessions) based on an export from your watch (as a TCX file).

If you interested by the story behind this web application, read the full story on [my blog](https://anitjourney.blog/the-straver). 
