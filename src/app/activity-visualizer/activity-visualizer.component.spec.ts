import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivityVisualizerComponent} from './activity-visualizer.component';

describe('VisualizerComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        ActivityVisualizerComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(ActivityVisualizerComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

});
