export class LapEditionData {
  public distance: number;

  constructor(distance: number) {
    this.distance = distance;
  }
}
