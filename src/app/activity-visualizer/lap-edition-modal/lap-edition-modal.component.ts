import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {LapEditionData} from './lap-edition-data';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-lap-edition-modal',
  templateUrl: './lap-edition-modal.component.html',
  styleUrls: ['./lap-edition-modal.component.scss']
})
export class LapEditionModalComponent implements OnInit {

  @Input() fromParent: LapEditionData | undefined;
  distanceControl = new FormControl(0,
    [Validators.required, Validators.min(0)]);

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
    this.distanceControl.setValue(this.fromParent!.distance);
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

  save(): void {
    this.activeModal.close(new LapEditionData(this.distanceControl.value?? 0));
  }
}
