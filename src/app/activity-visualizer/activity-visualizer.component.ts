import {Component} from '@angular/core';
import {saveAs} from 'file-saver';
import {faCheckCircle, faExclamationCircle} from '@fortawesome/free-solid-svg-icons';
import {formatDate} from '@angular/common';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {LapEditionModalComponent} from './lap-edition-modal/lap-edition-modal.component';
import {LapEditionData} from './lap-edition-modal/lap-edition-data';
import {XmlConversionService} from './xml-conversion/xml-conversion.service';
import {TcxService} from './tcx/tcx.service';


@Component({
  selector: 'app-activity-visualizer',
  templateUrl: './activity-visualizer.component.html',
  styleUrls: ['./activity-visualizer.component.scss']
})
export class ActivityVisualizerComponent {
  data: any;
  faExclamationCircle = faExclamationCircle;
  faCheckCircle = faCheckCircle;
  validXml = false;
  validTcx = false;
  coherentTimes = false;
  coherentDistances = false;
  logEntries: string[] = [];
  totalDistance = 0.0; // in meters
  totalDuration = 0; // in seconds
  pendingWork = false;

  constructor(
    private conversionService: XmlConversionService,
    private tcxService: TcxService,
    private modalService: NgbModal) {
  }

  processFile(file: File): void {
    this.pendingWork = true;
    const fileReader = new FileReader();
    fileReader.onload = () => {
      this.log('File uploaded');
      if (this.conversionService.isValidXml('' + fileReader.result)) {
        this.validXml = true;
        this.data = this.conversionService.xmlToJson('' + fileReader.result);
        this.data = this.tcxService.fixSmallGlitches(this.data);
        this.validTcx = this.tcxService.isValidTcx(this.data);
        this.refreshPagesInfo();
      } else {
        this.log('Uploaded file is not a valid XML');
        this.data = null;
      }
    };
    fileReader.onloadend = () => {
      this.pendingWork = false;
    };
    fileReader.readAsText(file);
  }

  fileUploaded(event: Event): void {
    if (event.target == null) {
      console.warn('No file provided');
      return;
    }
    const inputTag: any = event.target;
    if (inputTag.files == null || inputTag.files.length !== 1) {
      console.warn('Not exactly one file provided');
      return;
    }
    this.processFile(inputTag.files[0]);
  }

  downloadXmlFile(): void {
    this.pendingWork = true;
    const xml = this.conversionService.jsonToXml(this.data);
    const blob = new Blob([xml], {type: 'application/xml'});
    saveAs(blob, 'straver-out.xml');
    this.pendingWork = false;
    this.log('XML file downloaded');
  }

  deleteLap(index: number): void {
    this.pendingWork = true;
    this.data = this.tcxService.removeLap(this.data, index);
    this.refreshPagesInfo();
    this.pendingWork = false;
    this.log(`Lap ${index} deleted`);
  }

  mergeLap(index: number): void {
    this.pendingWork = true;
    this.data = this.tcxService.mergeLapWithNextOne(this.data, index);
    this.refreshPagesInfo();
    this.pendingWork = false;
    this.log(`Lap ${index} and ${index + 1} merged`);
  }

  autoSplitLap(index: number): void {
    this.pendingWork = true;
    const splitResult = this.tcxService.splitLap(this.data.TrainingCenterDatabase.Activities.Activity.Lap[index]);
    if (splitResult.length > 1) {
      this.data.TrainingCenterDatabase.Activities.Activity.Lap.splice(index, 1, splitResult);
    }
    this.refreshPagesInfo();
    this.pendingWork = false;
    this.log(`Lap ${index} auto split`);
  }

  deleteEmptyLaps(): void {
    this.pendingWork = true;
    this.data = this.tcxService.removeLapsWithNoDistance(this.data);
    this.refreshPagesInfo();
    this.pendingWork = false;
    this.log('Empty laps deleted');
  }

  fixTimes(): void {
    this.pendingWork = true;
    this.data = this.tcxService.fixIncoherenceInTimes(this.data);
    this.refreshPagesInfo();
    this.pendingWork = false;
    this.log('Times fixed');
  }

  fixDistances(): void {
    this.pendingWork = true;
    this.data = this.tcxService.fixIncoherenceInDistances(this.data);
    this.refreshPagesInfo();
    this.pendingWork = false;
    this.log('Distances fixed');
  }

  openLapEditModal(lapIndex: number, distance: number): void {
    const modalRef = this.modalService.open(LapEditionModalComponent, {centered: true});
    modalRef.componentInstance.fromParent = new LapEditionData(distance);
    modalRef.result.then(
      (data: LapEditionData) => {
        this.pendingWork = true;
        this.data = this.tcxService.changeLapDistance(this.data, lapIndex, data.distance);
        this.refreshPagesInfo();
        this.pendingWork = false;
        this.log(`Changed distance of lap ${lapIndex} from ${distance}m to ${data.distance}m.`);
      },
      () => console.log('Lap edition dismissed'));
  }

  reset(): void {
    this.data = null;
    this.validTcx = false;
    this.validXml = false;
    this.coherentDistances = false;
    this.coherentTimes = false;
    this.logEntries = [];
  }

  autoFixes(): void {
    this.pendingWork = true;
    this.data = this.tcxService.removeLapsWithNoDistance(this.data);
    this.data = this.tcxService.fixIncoherenceInDistances(this.data);
    this.data = this.tcxService.fixIncoherenceInTimes(this.data);
    this.refreshPagesInfo();
    this.pendingWork = false;
    this.log('Automatic fixes applied');
  }

  private refreshPagesInfo(): void {
    if (this.validTcx) {
      this.coherentTimes = this.tcxService.areTimesCoherent(this.data);
      this.coherentDistances = this.tcxService.areDistancesCoherent(this.data);
      this.totalDuration = this.tcxService.computeTotalDuration(this.data);
      this.totalDistance = this.tcxService.computeTotalDistance(this.data);
    } else {
      this.coherentTimes = false;
      this.coherentDistances = false;
      this.totalDistance = 0.0;
      this.totalDuration = 0;
    }
  }

  private log(message: string): void {
    console.log(message);
    this.logEntries.push(formatDate(Date.now(), 'short', 'en-US') + ': ' + message);
  }
}
