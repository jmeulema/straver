import {TestBed} from '@angular/core/testing';

import {XmlConversionService} from './xml-conversion.service';

describe('ConversionService', () => {
  let service: XmlConversionService;
  const simpleXml = '<?xml version="1.0" encoding="UTF-8"?>' +
    '<Employee type="human">' +
    '<id>1</id>' +
    '<name>Faisal</name>' +
    '<gender>Male</gender>' +
    '<mobile>514545</mobile>' +
    '</Employee>';

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(XmlConversionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(`should validate small xml properly`, () => {
    expect(service.isValidXml(simpleXml)).toBeTrue();
  });

  it(`should convert an xml to json and then to xml properly`, () => {
    expect(service.jsonToXml(service.xmlToJson(simpleXml))).toEqual(simpleXml);
  });

  it(`should convert small xml properly`, () => {
    expect(service.xmlToJson(simpleXml)).toEqual(
      {
        Employee: {
          attr: {type: 'human'},
          id: 1,
          name: 'Faisal',
          gender: 'Male',
          mobile: 514545
        }
      });
  });
});
