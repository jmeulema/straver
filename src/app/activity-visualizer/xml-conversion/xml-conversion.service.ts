import {Injectable} from '@angular/core';
import {XMLBuilder, XMLParser, XMLValidator} from 'fast-xml-parser';

@Injectable({
  providedIn: 'root'
})
export class XmlConversionService {

  private xmlBuilder: XMLBuilder = new XMLBuilder({
    attributeNamePrefix: '',
    attributesGroupName: 'attr', // default is false
    textNodeName: '#text',
    ignoreAttributes: false,
    format: false,
    indentBy: '  '
  });

  private xmlParser: XMLParser = new XMLParser({
    ignoreDeclaration: true,
    attributeNamePrefix: '',
    attributesGroupName: 'attr', // default is 'false'
    textNodeName: '#text',
    ignoreAttributes: false,
    allowBooleanAttributes: false,
    parseAttributeValue: true,
    trimValues: true,
    stopNodes: ['parse-me-as-string']
  });

  constructor() {
  }

  public isValidXml(xmlData: string): boolean {
    const validationResult = XMLValidator.validate(xmlData);
    if (validationResult === true) {
      return true;
    }
    console.error('Invalid XML', validationResult);
    return false;
  }

  public xmlToJson(xmlData: string): any {
    return this.xmlParser.parse(xmlData);
  }

  public jsonToXml(jsonData: string): string {
    return '<?xml version="1.0" encoding="UTF-8"?>' + this.xmlBuilder.build(jsonData);
  }
}
