import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TcxService {

  constructor() {
  }

  public isValidTcx(tcxData: any): boolean {
    if (tcxData == null) {
      return false;
    }
    if (!tcxData.TrainingCenterDatabase) {
      console.log('First node is not a TrainingCenterDatabase');
      return false;
    }
    if (!tcxData.TrainingCenterDatabase.Activities) {
      console.log('Missing an Activities node');
      return false;
    }
    if (!tcxData.TrainingCenterDatabase.Activities.Activity) {
      console.log('Missing an Activity node');
      return false;
    }
    if (tcxData.TrainingCenterDatabase.Activities.Activity instanceof Array) {
      console.log('More than one Activity node defined');
      return false;
    }
    if (!tcxData.TrainingCenterDatabase.Activities.Activity.Lap) {
      console.log('Missing an Lap nodes');
      return false;
    }
    const badLaps = tcxData.TrainingCenterDatabase.Activities.Activity.Lap
      .filter((lap: any) => !lap.Track || !Array.isArray(lap.Track.Trackpoint));
    if (badLaps.length > 0) {
      console.log(`Missing Trackpoint nodes in Lap(s) stating at: ${badLaps.map((l: any) => l.attr.StartTime)}`);
      return false;
    }
    return true;
  }

  public areTimesCoherent(tcxData: any): boolean {
    // start times of each lap + track point times -> always increasing
    const laps = tcxData.TrainingCenterDatabase.Activities.Activity.Lap;
    let time = Date.parse(laps[0].attr.StartTime);

    for (const lap of laps) {
      const lapDuration = lap.TotalTimeSeconds;
      const lapStartTime = Date.parse(lap.attr.StartTime);

      if (lapStartTime < time) {
        console.log(`Lap starting time ${lapStartTime} is before the current time ${time}`);
        return false;
      }

      time = lapStartTime;
      for (const trackpoint of lap.Track.Trackpoint) {
        const trackpointTime = Date.parse(trackpoint.Time);
        if (trackpointTime < time) {
          console.log(`Lap trackpoint time ${trackpointTime} is before the current time ${time}`);
          return false;
        }
        time = trackpointTime;
      }
      if (lapDuration * 1000 !== time - lapStartTime) {
        console.log(`Lap duration ${lapDuration} is not equal to the total tracked time of the lap ${time - lapStartTime}`);
        return false;
      }
    }
    return true;
  }

  public areDistancesCoherent(tcxData: any): boolean {
    // start distance of each lap + track point distance -> always increasing
    const laps = tcxData.TrainingCenterDatabase.Activities.Activity.Lap;
    let distance = 0.0;

    for (const lap of laps) {
      const lapDistance = lap.DistanceMeters;
      const lapStartDistance = lap.Track.Trackpoint[0].DistanceMeters;

      if (lapStartDistance < distance) {
        console.error(`Lap starting distance ${lapStartDistance} is before the current distance ${distance}`);
        return false;
      }

      distance = lapStartDistance;
      for (const trackpoint of lap.Track.Trackpoint) {
        const trackpointDistance = trackpoint.DistanceMeters;
        if (trackpointDistance < distance) {
          console.error(`Lap trackpoint distance ${trackpointDistance} is before the current distance ${distance}`);
          return false;
        }
        distance = trackpointDistance;
      }
      if (distance !== lapStartDistance + lapDistance) {
        console.error(`Lap started at ${lapStartDistance}m and was supposed to be ${lapDistance}m. We ended up at ${distance}m`);
        return false;
      }
    }
    return true;
  }

  public removeLap(tcx: any, index: number): any {
    if (index > -1) {
      tcx.TrainingCenterDatabase.Activities.Activity.Lap.splice(index, 1);
    }
    return tcx;
  }

  public removeLapsWithNoDistance(tcx: any): any {
    tcx.TrainingCenterDatabase.Activities.Activity.Lap =
      tcx.TrainingCenterDatabase.Activities.Activity.Lap.filter((l: { DistanceMeters: number; }) => l.DistanceMeters > 0);
    return tcx;
  }

  public fixIncoherenceInTimes(tcx: any): any {
    const laps = tcx.TrainingCenterDatabase.Activities.Activity.Lap;
    let time: number = Date.parse(laps[0].attr.StartTime);

    for (const lap of laps) {
      const lapOriginalDuration = lap.TotalTimeSeconds;
      const lapOriginalStartTime = Date.parse(lap.attr.StartTime);
      const lapDelta = lapOriginalStartTime - time;
      const lapAdjustedStartTime = time;
      lap.attr.StartTime = new Date(lapAdjustedStartTime).toISOString();
      console.log(`Lap data: start at ${new Date(lapOriginalStartTime).toISOString()} with delta of`
        + ` ${lapDelta}ms, changed to ${new Date(lapAdjustedStartTime).toISOString()}`);
      for (const trackpoint of lap.Track.Trackpoint) {
        const trackpointTime = Date.parse(trackpoint.Time);
        time = trackpointTime - lapDelta;
        trackpoint.Time = new Date(time).toISOString();
        console.log(` - Track point moved from ${new Date(trackpointTime).toISOString()} to ${trackpoint.Time}.`);
      }
      if (lapOriginalDuration * 1000 !== time - lapAdjustedStartTime) {
        console.log(`Lap duration ${lapOriginalDuration} is not equal to the total tracked time ` +
          `of the lap ${time - lapAdjustedStartTime}`);
        lap.TotalTimeSeconds = (time - lapAdjustedStartTime) / 1000.0;
      }
    }
    return tcx;
  }

  public fixIncoherenceInDistances(tcx: any): any {
    const laps = tcx.TrainingCenterDatabase.Activities.Activity.Lap;
    let distance = 0.0;

    for (let lap of laps) {
      lap = this.fixIncoherenceInDistancesOfLap(lap, distance);
      distance += lap.DistanceMeters;
    }
    return tcx;
  }

  public changeLapDistance(tcx: any, index: number, distance: number): any {
    console.log('Changing lap distance', index, distance);
    if (index > -1) {
      const lap = tcx.TrainingCenterDatabase.Activities.Activity.Lap[index];
      lap.DistanceMeters = distance;
      lap.Track.Trackpoint[lap.Track.Trackpoint.length - 1].DistanceMeters =
        lap.Track.Trackpoint[0].DistanceMeters + distance;

      tcx.TrainingCenterDatabase.Activities.Activity.Lap[index] =
        this.fixIncoherenceInDistancesOfLap(lap, lap.Track.Trackpoint[0].DistanceMeters);
    }
    return tcx;
  }

  public mergeLapWithNextOne(tcx: any, index: number): any {
    if (index < 0 || index + 1 >= tcx.TrainingCenterDatabase.Activities.Activity.Lap.length) {
      return tcx;
    }

    // move track points
    tcx.TrainingCenterDatabase.Activities.Activity.Lap[index].Track.Trackpoint =
      tcx.TrainingCenterDatabase.Activities.Activity.Lap[index].Track.Trackpoint.concat(
        tcx.TrainingCenterDatabase.Activities.Activity.Lap[index + 1].Track.Trackpoint);

    // update lap distance and duration
    tcx.TrainingCenterDatabase.Activities.Activity.Lap[index].DistanceMeters +=
      tcx.TrainingCenterDatabase.Activities.Activity.Lap[index + 1].DistanceMeters;
    tcx.TrainingCenterDatabase.Activities.Activity.Lap[index].TotalTimeSeconds +=
      tcx.TrainingCenterDatabase.Activities.Activity.Lap[index + 1].TotalTimeSeconds;

    // remove second lap
    tcx.TrainingCenterDatabase.Activities.Activity.Lap.splice(index + 1, 1);

    return tcx;
  }

  private fixIncoherenceInDistancesOfLap(lap: any, startDistance: number): any {
    let distance = startDistance;

    const lapStartDistance = distance;
    const lapDelta = distance - lap.Track.Trackpoint[0].DistanceMeters;
    const lapMaxDistance = distance + lap.DistanceMeters;
    console.log(`Lap data: start at ${lap.Track.Trackpoint[0].DistanceMeters}m with delta of ${lapDelta}m.`);

    for (const trackpoint of lap.Track.Trackpoint) {
      distance = Math.min(Math.max(distance, trackpoint.DistanceMeters + lapDelta), lapMaxDistance);
      console.log(` - Track point moved from ${trackpoint.DistanceMeters}m to ${distance}m.`);
      trackpoint.DistanceMeters = distance;
    }

    if (distance < lapStartDistance + lap.DistanceMeters) {
      console.log(` * Last track point updated from ${distance}m to ${lapStartDistance + lap.DistanceMeters}m (to match lap total distance).`);
      distance = lapStartDistance + lap.DistanceMeters;
      lap.Track.Trackpoint[lap.Track.Trackpoint.length - 1].DistanceMeters = distance;
    } else if (distance > lapStartDistance + lap.DistanceMeters) {
      console.log(` * Lap total distance updated from ${lap.DistanceMeters}m to ${distance - lapStartDistance}m (to match tracked distance in the lap).`);
      lap.DistanceMeters = distance - lapStartDistance;
    }
    return lap;
  }

  public computeTotalDuration(tcx: any): number {
    const laps = tcx.TrainingCenterDatabase.Activities.Activity.Lap;
    let totalDuration = 0;
    for (const lap of laps) {
      totalDuration += lap.TotalTimeSeconds;
    }
    return totalDuration;
  }

  public computeTotalDistance(tcx: any): number {
    const laps = tcx.TrainingCenterDatabase.Activities.Activity.Lap;
    let totalDistance = 0;
    for (const lap of laps) {
      totalDistance += lap.DistanceMeters;
    }
    return totalDistance;
  }

  public fixSmallGlitches(tcx: any): any {
    let activity = tcx.TrainingCenterDatabase.Activities.Activity;
    if (!Array.isArray(activity.Lap)) {
      // If only one Lap exist, convert it into an array anyway
      activity.Lap = new Array(activity.Lap);
    }
    for (const lap of activity.Lap) {
      if (Array.isArray(lap.Track)) {
        lap.Track = lap.Track[0]; // Polar sometime add a lonely Track>Trackpoint after all the other trackpoints
      }
      if (!Array.isArray(lap.Track.Trackpoint)) {
        lap.Track.Trackpoint = new Array(lap.Track.Trackpoint);
      }
      // Only keep the trackpoints that contain DistanceMeters and Time
      lap.Track.Trackpoint = lap.Track.Trackpoint.filter((trackpoint: any) => trackpoint.Time !== undefined && trackpoint.DistanceMeters !== undefined);
    }
    return tcx;
  }

  public splitLap(originalLap: any): any {
    const result = [];
    let lapStartIndex = 0;
    let lastDistanceChangeIndex = 0;
    debugger;

    for (let i = 1; i < originalLap.Track.Trackpoint.length -1; i++) {
      const currPoint = originalLap.Track.Trackpoint[i];
      const lastChangePoint = originalLap.Track.Trackpoint[lastDistanceChangeIndex];
      const lapStartPoint = originalLap.Track.Trackpoint[lapStartIndex];
      const currPointTime: Date = new Date(currPoint.Time);
      const lastChangePointTime: Date = new Date(lastChangePoint.Time);

      if (currPoint.DistanceMeters % 25 === 0
        && currPoint.DistanceMeters === lastChangePoint.DistanceMeters
        && currPoint.DistanceMeters > lapStartPoint.DistanceMeters
        && (currPointTime.getTime() - lastChangePointTime.getTime()) >= 3000) {
        // End of work lap: It has been more than 3 seconds that the distance has not changed: end of lap.
        result.push(this.extractLap(originalLap, lapStartIndex, i, 'Active'));
        lapStartIndex = i+1;
        lastDistanceChangeIndex = lapStartIndex;
      } else if (lastDistanceChangeIndex === lapStartIndex
        && i > (lapStartIndex +2)
        && currPoint.DistanceMeters > lapStartPoint.DistanceMeters
        && (new Date(currPoint.Time).getTime() - new Date(lapStartPoint.Time).getTime()) >= 3000) {
        result.push(this.extractLap(originalLap, lapStartIndex, i-2, 'Resting'));
        lapStartIndex = i-1;
        lastDistanceChangeIndex = lapStartIndex;
      } else if (currPoint.DistanceMeters !== lastChangePoint.DistanceMeters) {
        lastDistanceChangeIndex = i;
      }
    }
    result.push(this.extractLap(originalLap, lapStartIndex, originalLap.Track.Trackpoint.length - 1, 'Active'));
    return result;
  }

  private extractLap(originalLap: any, start: number, end: number, intensity: string): any {
    const startTime: Date = new Date(originalLap.Track.Trackpoint[start].Time);
    const startDistance: number = originalLap.Track.Trackpoint[start].DistanceMeters;
    const endTime: Date = new Date(originalLap.Track.Trackpoint[end].Time);
    const endDistance: number = originalLap.Track.Trackpoint[end].DistanceMeters;
    return {
      attr: {StartTime: startTime.toISOString()},
      DistanceMeters: endDistance - startDistance,
      Intensity: intensity,
      AverageHeartRateBpm: originalLap.AverageHeartRateBpm??0,
      TotalTimeSeconds: (endTime.getTime() - startTime.getTime()) / 1000,
      Track: {
        Trackpoint: originalLap.Track.Trackpoint.slice(start, end+1),
      }
    };
  }


}
