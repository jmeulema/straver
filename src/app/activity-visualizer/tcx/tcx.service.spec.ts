import {TestBed} from '@angular/core/testing';

import {TcxService} from './tcx.service';

describe('TcxService', () => {
  let service: TcxService;
  const smallTcx = {
    TrainingCenterDatabase: {
      Activities: {
        Activity: {
          Id: '2021-02-02T06:04:14.914Z',
          Lap: [
            {
              attr: {StartTime: '2021-02-27T14:41:20.068Z'},
              AverageHeartRateBpm: {Value: 127},
              Calories: 294,
              DistanceMeters: 500,
              Intensity: 'Active',
              MaximumHeartRateBpm: {Value: 151},
              MaximumSpeed: 1.0162601206037734,
              TotalTimeSeconds: 594,
              Track: {
                Trackpoint: [
                  {
                    DistanceMeters: 0.0,
                    HeartRateBpm: {Value: 81},
                    SensorState: 'Present',
                    Time: '2021-02-27T14:41:20.068Z',
                  },
                  {
                    DistanceMeters: 500.0,
                    HeartRateBpm: {Value: 81},
                    SensorState: 'Present',
                    Time: '2021-02-27T14:51:14.068Z',
                  }]
              }
            }]
        }
      }
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TcxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(`should validate small tcx properly`, () => {
    expect(service.isValidTcx(smallTcx)).toBeTrue();
  });

  it(`should check time coherence of a small tcx properly`, () => {
    expect(service.areTimesCoherent(smallTcx)).toBeTrue();
  });

  it(`should check distance coherence of a small tcx properly`, () => {
    expect(service.areDistancesCoherent(smallTcx)).toBeTrue();
  });

  it(`should fix the incoherence of the times`, () => {
    const txc = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 124,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-02T08:02:03.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 55,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T08:02:03.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T08:02:59.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    const expectedResult = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 123,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-02T06:02:03.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 56,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:59.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.fixIncoherenceInTimes(txc)).toEqual(expectedResult);
  });

  it(`should fix the incoherence of the distances (missing distance in trackpoints)`, () => {
    const txc = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 123,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 450.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-02T06:02:03.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 55,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 450.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 950.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:58.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    const expectedResult = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 123,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-02T06:02:03.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 55,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 1000.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:58.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.fixIncoherenceInDistances(txc)).toEqual(expectedResult);
  });

  it(`should fix the incoherence of the distances (distance going back)`, () => {
    const txc = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 123,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-02T06:02:03.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 55,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 450.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:48.000Z',
                    },
                    {
                      DistanceMeters: 1000.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:58.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    const expectedResult = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 123,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-02T06:02:03.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 55,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:48.000Z',
                    },
                    {
                      DistanceMeters: 1000.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:58.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.fixIncoherenceInDistances(txc)).toEqual(expectedResult);
  });

  it(`should fix the incoherence of the distances (distance going further and back again)`, () => {
    const txc = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 123,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-02T06:02:03.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 55,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 1025.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:48.000Z',
                    },
                    {
                      DistanceMeters: 1000.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:58.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    const expectedResult = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 123,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-02T06:02:03.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 55,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 1000.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:48.000Z',
                    },
                    {
                      DistanceMeters: 1000.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:58.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.fixIncoherenceInDistances(txc)).toEqual(expectedResult);
  });

  it(`should remove laps with no distance`, () => {
    const tcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:51:14.068Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-27T14:51:14.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 0,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 60,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:51:14.068Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T15:42:20.068Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.removeLapsWithNoDistance(tcx)).toEqual(smallTcx);
  });

  it(`should remove lap`, () => {
    const tcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:51:14.068Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-27T14:51:14.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 0,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 60,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:51:14.068Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T15:42:20.068Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.removeLap(tcx, 1)).toEqual(smallTcx);
  });

  it(`should merge laps`, () => {
    const txc = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 123,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    }]
                }
              },
              {
                attr: {StartTime: '2021-02-02T06:02:03.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 55,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:48.000Z',
                    },
                    {
                      DistanceMeters: 1000.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:58.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    const expectedResult = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-02T06:00:00.000Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 1000,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 178,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:00:00.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:03.000Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:48.000Z',
                    },
                    {
                      DistanceMeters: 1000.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-02T06:02:58.000Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.mergeLapWithNextOne(txc, 0)).toEqual(expectedResult);
  });

  it(`should increase lap distance`, () => {
    const tcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 400,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    },
                    {
                      DistanceMeters: 400.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:51:14.068Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.changeLapDistance(tcx, 0, 500)).toEqual(smallTcx);
  });

  it(`should decrease lap distance`, () => {
    const tcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 600,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    },
                    {
                      DistanceMeters: 600.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:51:14.068Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.changeLapDistance(tcx, 0, 500)).toEqual(smallTcx);
  });

  it(`should compute total distance properly`, () => {
    expect(service.computeTotalDistance(smallTcx)).toEqual(500);
  });

  it(`should compute total duration properly`, () => {
    expect(service.computeTotalDuration(smallTcx)).toEqual(594);
  });

  it(`should fix polar glitches in tcx`, () => {
    const tcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: [
                  {
                    Trackpoint: [
                      {
                        DistanceMeters: 0.0,
                        HeartRateBpm: {Value: 81},
                        SensorState: 'Present',
                        Time: '2021-02-27T14:41:20.068Z',
                      },
                      {
                        DistanceMeters: 500.0,
                        HeartRateBpm: {Value: 81},
                        SensorState: 'Present',
                        Time: '2021-02-27T14:51:14.068Z',
                      }]
                  },
                  {
                    Trackpoint:
                      {
                        DistanceMeters: 500.0,
                        HeartRateBpm: {Value: 81},
                        SensorState: 'Present',
                        Time: '2021-02-27T14:51:14.068Z',
                      }
                  }
                ]
              }]
          }
        }
      }
    };
    expect(service.fixSmallGlitches(tcx)).toEqual(smallTcx);
  });

  it(`should filter out trackpoint with not Time and/or DistanceMeters in tcx`, () => {
    const tcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: {
                  Trackpoint: [
                    {
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    },
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                    },
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    },
                    {
                      DistanceMeters: 500.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:51:14.068Z',
                    },
                    {
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.fixSmallGlitches(tcx)).toEqual(smallTcx);
  });

  it(`should fix polar glitches (single trackpoint) in tcx`, () => {
    const tcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: {
                  Trackpoint: {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    }
                }
              }]
          }
        }
      }
    };
    const fixedTcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.fixSmallGlitches(tcx)).toEqual(fixedTcx);
  });

  it(`should fix polar glitches (single lap) in tcx`, () => {
    const tcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap:
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    }]
                }
              }
          }
        }
      }
    };
    const fixedTcx = {
      TrainingCenterDatabase: {
        Activities: {
          Activity: {
            Id: '2021-02-02T06:04:14.914Z',
            Lap: [
              {
                attr: {StartTime: '2021-02-27T14:41:20.068Z'},
                AverageHeartRateBpm: {Value: 127},
                Calories: 294,
                DistanceMeters: 500,
                Intensity: 'Active',
                MaximumHeartRateBpm: {Value: 151},
                MaximumSpeed: 1.0162601206037734,
                TotalTimeSeconds: 594,
                Track: {
                  Trackpoint: [
                    {
                      DistanceMeters: 0.0,
                      HeartRateBpm: {Value: 81},
                      SensorState: 'Present',
                      Time: '2021-02-27T14:41:20.068Z',
                    }]
                }
              }]
          }
        }
      }
    };
    expect(service.fixSmallGlitches(tcx)).toEqual(fixedTcx);
  });

  it(`should auto detect laps inside a lap`, () => {
    const lapData = {
      attr: { StartTime: '2021-02-02T06:00:00.000Z' },
      DistanceMeters: 50,
      TotalTimeSeconds: 12,
      AverageHeartRateBpm: {Value: 127},
      Track: {
        Trackpoint: [
          {
            DistanceMeters: 0,
            Time: '2021-02-02T06:00:00.000Z',
          },
          {
            DistanceMeters: 0,
            Time: '2021-02-02T06:00:01.000Z',
          },
          {
            DistanceMeters: 20,
            Time: '2021-02-02T06:00:03.000Z',
          },
          {
            DistanceMeters: 25,
            Time: '2021-02-02T06:00:04.000Z',
          },
          {
            DistanceMeters: 25,
            Time: '2021-02-02T06:00:06.000Z',
          },
          {
            DistanceMeters: 25,
            Time: '2021-02-02T06:00:07.000Z',
          },
          {
            DistanceMeters: 25,
            Time: '2021-02-02T06:00:08.000Z',
          },
          {
            DistanceMeters: 25,
            Time: '2021-02-02T06:00:09.000Z',
          },
          {
            DistanceMeters: 25,
            Time: '2021-02-02T06:00:09.100Z',
          },
          {
            DistanceMeters: 25,
            Time: '2021-02-02T06:00:11.000Z',
          },
          {
            DistanceMeters: 25,
            Time: '2021-02-02T06:00:12.000Z',
          },
          {
            DistanceMeters: 30,
            Time: '2021-02-02T06:00:13.000Z',
          },
          {
            DistanceMeters: 50,
            Time: '2021-02-02T06:00:14.000Z',
          },
          {
            DistanceMeters: 50,
            Time: '2021-02-02T06:00:15.000Z',
          }
        ],
      },
    };
    const expected = [
      {
        attr: { StartTime: '2021-02-02T06:00:00.000Z' },
        DistanceMeters: 25,
        TotalTimeSeconds: 7,
        Intensity: 'Active',
        AverageHeartRateBpm: {Value: 127},
        Track: {
          Trackpoint: [
            {
              DistanceMeters: 0,
              Time: '2021-02-02T06:00:00.000Z',
            },
            {
              DistanceMeters: 0,
              Time: '2021-02-02T06:00:01.000Z',
            },
            {
              DistanceMeters: 20,
              Time: '2021-02-02T06:00:03.000Z',
            },
            {
              DistanceMeters: 25,
              Time: '2021-02-02T06:00:04.000Z',
            },
            {
              DistanceMeters: 25,
              Time: '2021-02-02T06:00:06.000Z',
            },
            {
              DistanceMeters: 25,
              Time: '2021-02-02T06:00:07.000Z',
            }
          ],
        },
      },
      {
        attr: { StartTime: '2021-02-02T06:00:08.000Z' },
        DistanceMeters: 0,
        TotalTimeSeconds: 3,
        Intensity: 'Resting',
        AverageHeartRateBpm: {Value: 127},
        Track: {
          Trackpoint: [
            {
              DistanceMeters: 25,
              Time: '2021-02-02T06:00:08.000Z',
            },
            {
              DistanceMeters: 25,
              Time: '2021-02-02T06:00:09.000Z',
            },
            {
              DistanceMeters: 25,
              Time: '2021-02-02T06:00:09.100Z',
            },
            {
              DistanceMeters: 25,
              Time: '2021-02-02T06:00:11.000Z',
            }
          ],
        },
      },
      {
        attr: { StartTime: '2021-02-02T06:00:12.000Z' },
        DistanceMeters: 25,
        TotalTimeSeconds: 3,
        Intensity: 'Active',
        AverageHeartRateBpm: {Value: 127},
        Track: {
          Trackpoint: [
            {
              DistanceMeters: 25,
              Time: '2021-02-02T06:00:12.000Z',
            },
            {
              DistanceMeters: 30,
              Time: '2021-02-02T06:00:13.000Z',
            },
            {
              DistanceMeters: 50,
              Time: '2021-02-02T06:00:14.000Z',
            },
            {
              DistanceMeters: 50,
              Time: '2021-02-02T06:00:15.000Z',
            }
          ],
        },
      }
    ];
    expect(service.splitLap(lapData)).toEqual(expected);
  });

  it(`should auto detect no laps inside the lap`, () => {
    const lap = {
      attr: {StartTime: '2021-02-27T14:41:20.068Z'},
      AverageHeartRateBpm: {Value: 127},
      DistanceMeters: 500,
      Intensity: 'Active',
      TotalTimeSeconds: 594,
      Track: {
        Trackpoint: [
          {
            DistanceMeters: 0.0,
            HeartRateBpm: {Value: 81},
            SensorState: 'Present',
            Time: '2021-02-27T14:41:20.068Z',
          },
          {
            DistanceMeters: 500.0,
            HeartRateBpm: {Value: 81},
            SensorState: 'Present',
            Time: '2021-02-27T14:51:14.068Z',
          }]
      }
    };
    expect(service.splitLap(lap)).toEqual(new Array(lap));
  });

});
