import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ActivityVisualizerComponent} from './activity-visualizer/activity-visualizer.component';
import {OverviewComponent} from './overview/overview.component';
import {ContactComponent} from './contact/contact.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/overview',
    pathMatch: 'full'
  },
  {
    path: 'overview',
    component: OverviewComponent
  },
  {
    path: 'activity-visualizer',
    component: ActivityVisualizerComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  // Using hashes in order to make it work with gitlab pages despite the open issue: https://gitlab.com/gitlab-org/gitlab-pages/-/issues/23
  exports: [RouterModule]
})
export class AppRoutingModule { }
