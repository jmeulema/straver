import {DurationFormatPipe} from './duration-format.pipe';

describe('DurationFormatPipe', () => {
  it('create an instance', () => {
    const pipe = new DurationFormatPipe();
    expect(pipe).toBeTruthy();
  });

  it('should properly convert', () => {
    const pipe = new DurationFormatPipe();
    expect(pipe.transform(1)).toEqual('00:01');
    expect(pipe.transform(12)).toEqual('00:12');
    expect(pipe.transform(60)).toEqual('01:00');
    expect(pipe.transform(61)).toEqual('01:01');
    expect(pipe.transform(601)).toEqual('10:01');
    expect(pipe.transform(3600)).toEqual('1:00:00');
    expect(pipe.transform(3661)).toEqual('1:01:01');
  });
});
