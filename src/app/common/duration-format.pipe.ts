import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'durationFormat'
})
export class DurationFormatPipe implements PipeTransform {

  transform(valueInSeconds: number): any {
    const seconds = Math.floor(valueInSeconds % 60);
    const minutes = Math.floor(valueInSeconds / 60) % 60;
    const hours = Math.floor(valueInSeconds / 3600);

    return ((hours > 0) ? hours.toString() + ':' : '') +
      ((minutes > 9) ? '' : '0') + minutes.toString() + ':' +
      ((seconds > 9) ? '' : '0') + seconds.toString();
  }

}
