import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {DurationFormatPipe} from './common/duration-format.pipe';
import {ReactiveFormsModule} from '@angular/forms';
import {LapEditionModalComponent} from './activity-visualizer/lap-edition-modal/lap-edition-modal.component';
import {ActivityVisualizerComponent} from './activity-visualizer/activity-visualizer.component';
import {OverviewComponent} from './overview/overview.component';
import {ContactComponent} from './contact/contact.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    ActivityVisualizerComponent,
    DurationFormatPipe,
    LapEditionModalComponent,
    OverviewComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    LapEditionModalComponent
  ]
})
export class AppModule {
}
