import {Component} from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Recipy';
  subTitle = 'Follow and share your best recipes';
  currentYear = new Date().getFullYear();
  navbarCollapsed = true;
}
